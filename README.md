# Social Media website with React, Node.js

## Website: 
[https://social-media-dmd.netlify.app/](https://social-media-dmd.netlify.app/)

## Features
- Sign up, log in (hash password, token)    
- Make a new post         
- Like, comment on a posts       
- To be developed: Follow and Unfollow; Private/public posts; Profile customization; Post picture, audio; Direct Messaging; Share; Save post; Report post/user; Two-factor authenticator; etc.  

## Tech stack
- React
- Node.js
- GraphQL 
- MongoDB for database
- Heroku + Netlify + Git for cloud deployment

## Set up on local server
```bash
git clone git@bitbucket.org:xdmd/web.git
cd web
npm start
```
If you have all the required packages (node, MongoDB, etc.), the server now starts at http://localhost:5000  

Next, open a new terminal to start client
```bash
cd web/client
npm start
```
The website is at http://localhost:3000  
You can register a new username, log in, make a post, like and comment.  

#### Tag
full-stack, front-end, back-end, Node.js, GraphQL, MongoDB, React, Heroku    

![Fail to register as username already exists] (/images/Register.PNG)   
![Fail to log in with wrong credentials](/images/Login.PNG)   
![Create a new post](/images/CreatePost.PNG)   
![Like/unlike and comment on a post](/images/LikeComment.PNG) 